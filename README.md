# Git Crash Course

## Outline

The workshop covers commiting, branching, merging, conflicts and pull requests.

## Prepare for the workshop

1. Install git

   Instructions here:
   https://www.atlassian.com/git/tutorials/install-git

2. If you don't already have one, a bitbucket account with your fusion email. I'll invite you to the repo by that email unless you give me another one.

   https://bitbucket.org/account/signup/create

3. Clone the repo:

   In the terminal create a `projects` or `workspace` folder if you don't already have one.  
   `cd` into that folder and run `git clone https://[[YOUR USERNAME]]@bitbucket.org/willfusion/git-workshop.git`.  
   You should now see a folder callled `git-workshop`.  
   When you `cd` inside and you should see three text files.

Optional steps:

4. Install a text editor.

   I highly recommend VS Code. It has a greate git integration out of the box.
   https://code.visualstudio.com/
   Also open the command panel with `CMD/CTRL + SHIFT + P` and run `Shell Command: Install 'code' command in PATH` so that vs code is avaliable in the terminal.
   Also if you have VS Code install the Git Lens extension (I'll be using it in my demos).
   https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

5. Change git's default editor in the terminal. Default is vim :scream:.

   `git config --global core.editor "code --wait"` // VS Code my favourite  
   `git config --global core.editor "subl -n -w"` // Sublime  
   `git config --global core.editor "atom --wait"` // Atom

6. Make git a little easier on the eye. Type in the terminal. :nail_care:

   git config --global color.ui auto

## A note to future readers

This workshop requires a facilitator to make changes to the remote repo while participants work through the exercises. If you would like to do something similar then I recommend the [gitit](https://github.com/jlord/git-it-electron) course as you can do it on your own.
