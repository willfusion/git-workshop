## Exercise 1

Goal: Add a commit to the master branch

```bash
# 0. Clone from github
$ git clone https://willfusion@bitbucket.org/willfusion/git-workshop.git
$ cd git-workshop

# 2. Make a new file with your name
$ echo "Name: [[YOUR NAME]]" > your-name.txt
# For example:
$ echo "Name: Will" > will.txt

# 3. See your status, your-name.txt is untracked
$ git status

# 4. Stage the new file
$ git add your-name.txt

# 5. git status, your-name.txt is "to be committed"
$ git status

# 6. commit
$ git commit -m "Add your-name.txt"

# 7. See the history
$ git log

# 8. Add a nice alias for log
$ git config --global alias.l "log --graph --pretty=oneline --abbrev-commit --decorate"
$ git l
```

Source: http://www.draconianoverlord.com/git-workshop.html
