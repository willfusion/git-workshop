## Exercise 2

Goal: Undo line changes

```bash
# 1. Add a line to your-name.txt, see the status
$ echo "line2" >> your-name.txt
$ git status

# 2. See the change
$ git diff

# 3. Undo your change, line2 is removed, nothing to commit
$ git checkout your-name.txt
$ git status

# 4. Re-add line 2 and now stage the change
$ echo "line2" >> your-name.txt
$ git add your-name.txt

# 5. See the staged change
$ git diff --staged

# 6. Note regular diff does not show changes
$ git diff

# diff          == working <-> staged
# diff --staged == staged <-> HEAD

# 7. Unstage the change, your-name.txt is back to modified
$ git reset new.txt
$ git status

# 8. Undo the change, line2 is removed, nothing to commit
$ git checkout new.txt
$ git status
```

Source: http://www.draconianoverlord.com/git-workshop.html
