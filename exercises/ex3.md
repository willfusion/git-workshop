## Exercise 3

Goal: Undo file changes

```bash
# 1. Add, stage a new file, see it staged
$ echo "line1" > new.txt
$ git add new.txt
$ git status

# 2. Unstage the new file, new.txt is back to untracked
$ git reset new.txt
$ git status
$ rm new.txt

# 3. Remove your-name.txt, it is shown as deleted
$ rm your-name.txt
$ git status

# 4. Stage the removal, it is shown as "to be committed"
$ git rm your-name.txt
$ git status

# 5. Unstage the removal (HEAD is important, try without it)
$ git reset HEAD your-name.txt
$ git status

# 6. Restore your-name.txt in working copy
$ git checkout your-name.txt
$ git status
```

Source: http://www.draconianoverlord.com/git-workshop.html
