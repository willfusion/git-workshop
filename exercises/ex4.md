## Exercise 4

Goal: Make a branch and merge in changes that happened on remote

```bash
# 1. See your branches
$ git branch

# 2. Make a new branch, see you're on the new one
$ git checkout -b your-name-branch
$ git branch -v

# 3. Make a change
$ echo "Favourite Movie: [[YOUR FAV MOVIE]]" >> your-name.txt
$ git add your-name.txt
$ git commit -m "Add fav movie to your-name.txt."

# 4. Go back to master
$ git checkout master

# 5. Pull new exercises from remote
$ git pull

# 6. Note in git log, the fav movie changes are not in master
$ git l

# 7. Checkout your branch
$ git checkout your-name-branch

# 7. Merge in master, see the new exercises and history
$ git merge master
$ ls exercises
$ git l
```

Source: http://www.draconianoverlord.com/git-workshop.html
