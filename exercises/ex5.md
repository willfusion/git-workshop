## Exercise 5

Goal: Fix up a previous commit

```bash
# 1. Add line4 with a typo, commit
$ echo "Birthplace: [[YOUR BIRTHPLACE]]" >> your-name.txt
$ git commit -a -m "Add birthplace to your-name.txt"

# 2. Note our history
$ git l
# sha-a "Add birthplace to your-name.txt."

# 3. Fix our typo, amend the commit
$ sed -e -i s/Birthplace/Hometown/ your-name.txt
$ git commit -a --amend

# 4. Note history, there is one, different "line4" commit
$ git l
# sha-a' "Add hometown to your-name.txt"

# commit sha = hash(timestamp, author, message, repo contents)
# amending always changing the last commit's sha

# 7. Note history
$ git l

# 8. Nothing is lost
$ git reflog

# Shows what "HEAD" was X commits ago
# Find 'commit (amend): Add hometown to your-name.txt'
# Find 'commit: Add birthplace to your-name.txt'

```

Source: http://www.draconianoverlord.com/git-workshop.html
