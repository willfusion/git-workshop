## Exercise 6

Goal: Resolve a conflict and make a pull request

```bash
# 1. Checkout master
$ git checkout master

# 2. Pull changes from remote
$ git pull

# 3. Note in git log the new commits
$ git l

# And notice that a line was added to your-name.txt
$ cat your-name.txt

# 4. Checkout your branch
$ git checkout your-name-branch

# 5. Merge in master
$ git merge master

# 6. Check the status
$ git status

# Notice that favourite-foods.txt (no conflicts) is already staged.

# 6. See only the conflicts
$ git diff

# 8. Examine each version of your-name.txt
$ git show :1:your-name.txt # original version
$ git show :2:your-name.txt # master's version
$ git show :3:your-name.txt # your-name-branch's version

# 9. Resolve the conflicts in your text editor and see what we changed
$ git diff

# 10. Commit, use the default commit message
$ git add your-name.txt
$ git commit
$ git l

# 11. Add your favourite food to favourite-food.txt
$ echo "[[ YOUR NAMES ]]'s favourite food is: [[ FAVOURITE FOOD ]]"
$ git add favourite-food.txt
$ git commit -m "Add [[ YOUR NAMES ]]'s favourite food."

# 11. Push changes to remote
$ git push

# 12. Read the error and do what it says
$ git push --set-upstream origin your-name-branch

# Read the message, open the link to create a PR and follow the instructions:
# remote: Create pull request for your-name-branch:
# remote: https://bitbucket.org/fusionsport/human-performance-framework/pull-requests/new?source=your-name-branch-version&t=1
# Resolve any conflicts ???
# Wait for Will to approve your PR.
# Yay, it was approved. Now merge it!
```

Source: http://www.draconianoverlord.com/git-workshop.html
